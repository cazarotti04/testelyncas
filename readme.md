Banco de dados:
	Firebase - Utilizei tal plataforma por ter bastante integra��o com os servi�os do Google, facilitando o gerenciamento de logins e no armazenamento de dados
	
Front-end:
	Angular - Utilizei por ter maior familiaridade com o framework, al�m da facilidade que o pacote prov�m para trabalhar com componentes
	Material - Como pedido no readme do teste, utilizei a framework material io para trabalhar a estiliza��o das p�ginas
	
Back-end:
	.Net Core - Utilizei esta framework por ter mais experi�ncia do que o .Net Framework, conseguindo projetar melhor o back-end da aplica��o em classes e servi�os bem espec�ficos. Vale ressaltar que, como n�o tenho conhecimento sobre qual tipo de servidor esta aplica��o rodaria, � vantajoso utilizar o .Net Core por ser multi-plataforma
	
========================================================================================

As dificuldades que eu enfrentei, em sua maioria, foram devido � utilizar novas tecnologias, principalmente com rela��o ao Material, que achei um tanto quanto diferente do que eu estava acostumado e tive uns contratempos para alinhar o que conseguia ou tentar deixar a tela bem enxuta. Com rela��o ao firebase precisei de algumas horas apenas para poder entender a maneira como a plataforma funciona e como ela integra os servi�os do google a fim de facilitar, por exemplo, a autentica��o no sistema.

Sobre JavaScript n�o tive muita dificuldade, n�o exigiam processos complexos no client-side para causar um impacto significativo no desenvolvimento.

No back-end tive bastante facilidade, todas as consultas retornam com uma Classe de response, que indicam sucesso ou n�o da consulta, um objeto retornando caso fosse uma consulta GET, um errorMessage, a fim de apontar diretamente o erro para facilitar sua identifica��o e corre��o, todas as exce��es s�o tratadas. Como disse acima, classes e servi�os est�o muito espec�ficos, sem redund�ncia de c�digo.

========================================================================================

Tempo de desenvolvimento

	Item 1 e 7 - 6 horas (2 horas de estudo sobre a plataforma Firebase e API's do Google + 3 horas para desenvolvimento da l�gica de login e estado de autentica��o e redirecionamento de rotas no sistema + 1 hora para trabalhar no layout base da aplica��o)
	Item 2 - 2 horas (1 para o desenvolvimento da parte l�gica do cabe�alho + 1 hora para ajustar layout do header)
	Item 3 - 6 horas (2 horas para o desenvolvimento do controlador, servi�o(onde fiz valida��o para n�o realizar consulta com par�metro vazio), modelo da busca e tratamento de exce��es, 30 minutos para desenvolvimento de um simples teste unit�rio para esse servi�o + 3 horas e 30 minutos para receber os dados no front-end e criar uma estrutura de lista com cards para exibi��o dos livros na tela)
	Item 4 - 4 horas (3 horas para o desenvolvimento do controlador, servi�o e modelo de favorito e tratamento das exce��es + 1 hora para desenvolver a l�gica que monta o objeto e faz a requisi��o post. Eu utilizei a lista de livros para inserir um botao de adicionar ao favoritos)
	Item 5 - 3 horas e 30 minutos (2 horas para o desenvolvimento da fun��o que recebe o post no controlador e servi�o com valida��es que remove o objeto no banco de dados do firebase + 30 minutos para desenvolvimento de simples testes + 1 hora para desenvolver a l�gica que faz a chamada delete para o servidor e incluir o bot�o 'Remover Favorito' para cada item da lista de favoritos e reutilizar a lista anteriormente criada mudando alguns detalhes)
	Item 6 - 30 minutos (30 minutos para implementa��o do pacote SweetAlert2 nos pontos necess�rios)
	
	Total: +- 22 horas

========================================================================================

Para executar o projeto:

	Requisitos:
		- Ter o nodeJs instalado em sua m�quina
			- Link: https://nodejs.org/en/download/
			- Vers�o que utilizei no desenvolvimento: v12.13.1
		- Ter o .Net Core Sdk instalado em sua m�quina
			- Link: https://dotnet.microsoft.com/download
			- Vers�o que utilizei no desenvolvimento: 3.1.101
			- Para o SDK do .Net necessariamente deve ser uma vers�o 3.1.x

	Voc� pode executar o processo de forma autom�tica com os arquivos bat que disponibilizei na raiz do projeto, eles ir�o:
		- Para Web: 
			- runWebInstaller.bat: entrar no diret�rio do projeto Web, executar o comando 'npm install';
			- runWebStart.bat: entrar no diret�rio do projeto Web, executar o comando 'npm start' para iniciar a execu��o do servidor web;
		- Para WebApi: entrar no diret�rio do projeto WebApi, executar o comando 'dotnet build -c Release' e em seguinda entrar na pasta do build e iniciar o execut�vel da aplica��o do back-end;
		
	Rode primeiro o runWebInstaller.bat, depois o runWebStart.bat e o runWebApi.bat independente da ordem!
	
	Se preferir tamb�m, pode fazer estes mesmos passos manualmente...
	
		- Para Web: entre no diret�rio Web, localizado na raiz da solu��o, atrav�s do cmd, power shell ou bash de sua escolha. Em seguida digite o comando 'npm install' e, quando finalizado, 'npm start';
		- Para WebApi: entre no diret�rioWebAppi, localizado na raiz da solu���o, atrav�s do cmd, power shell ou bash de sua escolha. Em seguida digite o comando 'dotnet build -c Release'. Quando finalizado navegue atrav�s das pastas bin/Release/netcoreapp3.1 e inicie o arquivo WebApi.exe.