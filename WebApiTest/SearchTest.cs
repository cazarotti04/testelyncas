﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using WebApi.Firebase;
using WebApi.Services;

namespace WebApiTest
{
    class SearchTest
    {
        [SetUp]
        public void Setup()
        {
            
        }

        [Test]
        public void GetTest()
        {
            //Busca Valida
            FirebaseResponse response;
            string validAPIKEY = "AIzaSyCqSxp1L1NJivvpE_SMvjkGnlRwngLHMDc";
            string validBookName = "teste";

            response = SearchService.Instance.SearchVolumes(validAPIKEY, validBookName);

            if (!response.Success)
                Assert.Fail();

            //Busca sem APIKEY
            response = SearchService.Instance.SearchVolumes("", validBookName);

            if (response.Success)
                Assert.Fail();

            //Busca sem APIKEY
            response = SearchService.Instance.SearchVolumes(null, validBookName);

            if (response.Success)
                Assert.Fail();

            //Busca sem BookName
            response = SearchService.Instance.SearchVolumes(validAPIKEY, "");

            if (response.Success)
                Assert.Fail();

            //Busca sem BookName
            response = SearchService.Instance.SearchVolumes(validAPIKEY, null);

            if (response.Success)
                Assert.Fail();

            Assert.Pass();
        }
    }
}
