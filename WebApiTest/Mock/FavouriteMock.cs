﻿using System;
using System.Collections.Generic;
using System.Text;
using WebApi.Models;

namespace WebApiTest.Mock
{
    public class FavouriteMock
    {
        public Favourite validPost { get; set; }

        public Favourite validDelete { get; set; }

        public FavouriteMock()
        {
            string guid = Guid.NewGuid().ToString();
            validPost = new Favourite()
            {
                userId = "lyncas",
                bookDescription = "Book Description",
                bookId = "Book Id",
                bookPhotoUrl = "Boook Photo Url",
                bookUrl = "Book Url",
                pageCount = 100,
                favouriteGuid = guid,
                bookName = "Book Name"
            };

            validDelete = new Favourite()
            {
                userId = "lyncas",
                bookDescription = "Book Description",
                bookId = "Book Id",
                bookPhotoUrl = "Boook Photo Url",
                bookUrl = "Book Url",
                pageCount = 100,
                favouriteGuid = guid,
                bookName = "Book Name"
            };
        }
    }
}
