using NUnit.Framework;
using System;
using System.Collections.Generic;
using WebApi.Firebase;
using WebApi.Models;
using WebApi.Services;
using WebApiTest.Mock;

namespace WebApiTest
{
    public class FavouriteTest
    {
        FavouriteMock mock;

        [SetUp]
        public void Setup()
        {
            mock = new FavouriteMock();
        }

        [Test]
        public void TestePostDelete()
        {
            FirebaseResponse response;

            response = FavouriteService.Instance.NewFavourite(mock.validPost);

            if (!response.Success)
                Assert.Fail();

            response = FavouriteService.Instance.DeleteFavourite(mock.validDelete.favouriteGuid, mock.validDelete.userId);

            if (!response.Success)
                Assert.Fail();

            response = FavouriteService.Instance.Favourites(mock.validPost.userId);

            foreach(var item in response.objRetorno as List<Favourite>)
            {
                if (item.favouriteGuid == mock.validPost.favouriteGuid)
                    Assert.Fail();
            }
            Assert.Pass();
        }

        [Test]
        public void TestePost()
        {
            //Cadastro v�lido
            FirebaseResponse response;

            response = FavouriteService.Instance.NewFavourite(mock.validPost);

            if (!response.Success)
                Assert.Fail();

            //Cadastro sem PhotoURL
            string urlValida = mock.validPost.bookPhotoUrl;
            mock.validPost.bookPhotoUrl = null;

            response = FavouriteService.Instance.NewFavourite(mock.validPost);
            if (response.Success)
                Assert.Fail();

            mock.validPost.bookPhotoUrl = urlValida;

            //Cadastro sem PhotoURL
            urlValida = mock.validPost.bookPhotoUrl;
            mock.validPost.bookPhotoUrl = "";

            response = FavouriteService.Instance.NewFavourite(mock.validPost);
            if (response.Success)
                Assert.Fail();

            mock.validPost.bookPhotoUrl = urlValida;

            //Cadastro sem UserID
            string userValido = mock.validPost.userId;
            mock.validPost.userId = "";

            response = FavouriteService.Instance.NewFavourite(mock.validPost);
            if (response.Success)
                Assert.Fail();

            mock.validPost.userId = userValido;

            //Cadastro sem UserID
            userValido = mock.validPost.userId;
            mock.validPost.userId = null;

            response = FavouriteService.Instance.NewFavourite(mock.validPost);
            if (response.Success)
                Assert.Fail();

            mock.validPost.userId = userValido;

            //Cadastro sem BookID
            string bookIdValido = mock.validPost.bookId;
            mock.validPost.bookId = null;

            response = FavouriteService.Instance.NewFavourite(mock.validPost);
            if (response.Success)
                Assert.Fail();

            mock.validPost.bookId = bookIdValido;

            //Cadastro sem BookID
            bookIdValido = mock.validPost.bookId;
            mock.validPost.bookId = "";

            response = FavouriteService.Instance.NewFavourite(mock.validPost);
            if (response.Success)
                Assert.Fail();

            mock.validPost.bookId = bookIdValido;

            //Cadastro sem BookUrl
            string bookUrlValido = mock.validPost.bookUrl;
            mock.validPost.bookUrl = "";

            response = FavouriteService.Instance.NewFavourite(mock.validPost);
            if (response.Success)
                Assert.Fail();

            mock.validPost.bookUrl = bookUrlValido;

            //Cadastro sem BookUrl
            bookUrlValido = mock.validPost.bookUrl;
            mock.validPost.bookUrl = null;

            response = FavouriteService.Instance.NewFavourite(mock.validPost);
            if (response.Success)
                Assert.Fail();

            mock.validPost.bookUrl = bookUrlValido;

            //Cadastro sem BookName
            string bookNameValido = mock.validPost.bookName;
            mock.validPost.bookName = "";

            response = FavouriteService.Instance.NewFavourite(mock.validPost);
            if (response.Success)
                Assert.Fail();

            mock.validPost.bookName = bookNameValido;

            //Cadastro sem BookName
            bookNameValido = mock.validPost.bookName;
            mock.validPost.bookName = null;

            response = FavouriteService.Instance.NewFavourite(mock.validPost);
            if (response.Success)
                Assert.Fail();

            mock.validPost.bookName = bookNameValido;

            //Pass
            Assert.Pass();
        }

        [Test]
        public void DeleteTest()
        {
            //Delete Valido
            FirebaseResponse response;

            response = FavouriteService.Instance.DeleteFavourite(mock.validDelete.favouriteGuid, mock.validDelete.userId);

            if (!response.Success)
                Assert.Fail();

            //Delete sem GUID
            string guidValido = mock.validDelete.favouriteGuid;
            mock.validDelete.favouriteGuid = "";

            response = FavouriteService.Instance.DeleteFavourite(mock.validDelete.favouriteGuid, mock.validDelete.userId);

            if (response.Success)
                Assert.Fail();

            mock.validDelete.favouriteGuid = guidValido;

            //Delete sem GUID
            guidValido = mock.validDelete.favouriteGuid;
            mock.validDelete.favouriteGuid = null;

            response = FavouriteService.Instance.DeleteFavourite(mock.validDelete.favouriteGuid, mock.validDelete.userId);

            if (response.Success)
                Assert.Fail();

            mock.validDelete.favouriteGuid = guidValido;

            //Delete sem UserID
            string userIdValido = mock.validDelete.userId;
            mock.validDelete.userId = null;

            response = FavouriteService.Instance.DeleteFavourite(mock.validDelete.favouriteGuid, mock.validDelete.userId);

            if (response.Success)
                Assert.Fail();

            mock.validDelete.userId = userIdValido;

            //Delete sem UserID
            userIdValido = mock.validDelete.userId;
            mock.validDelete.userId = "";

            response = FavouriteService.Instance.DeleteFavourite(mock.validDelete.favouriteGuid, mock.validDelete.userId);

            if (response.Success)
                Assert.Fail();

            mock.validDelete.userId = userIdValido;

            //Pass
            Assert.Pass();
        }

        [Test]
        public void GetTest()
        {
            //Delete Valido
            FirebaseResponse response;
            string userIdValido = "123lyncas321";

            response = FavouriteService.Instance.Favourites(userIdValido);

            if (!response.Success)
                Assert.Fail();

            //Delete sem UserId
            response = FavouriteService.Instance.Favourites("");
            if (response.Success)
                Assert.Fail();

            //Delete sem UserId
            response = FavouriteService.Instance.Favourites("");
            if (response.Success)
                Assert.Fail();

            //Pass
            Assert.Pass();
        }
    }
}