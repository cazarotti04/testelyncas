﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WebApi.Utils;

namespace WebApi.Firebase
{
    public class FirebaseRequest
    {
        private HttpMethod Method { get; set; }
 
        private string JSON { get; set; }

        private string Uri { get; set; }

        public FirebaseResponse Execute()
        {
            Uri requestURI;
            JToken parsedJSON;
            string json;

            requestURI = new Uri(Uri);

            if (!string.IsNullOrEmpty(JSON))
            {
                parsedJSON = JToken.Parse(JSON);
                json = parsedJSON.ToString();
            }
            else
                json = "";

            var response = RequestHelper(Method, requestURI, json);
            response.Wait();
            var result = response.Result;

            var firebaseResponse = new FirebaseResponse()
            {
                HttpResponse = result,
                ErrorMessage = result.StatusCode.ToString() + " : " + result.ReasonPhrase,
                Success = response.Result.IsSuccessStatusCode
            };

            if (Method.Equals(HttpMethod.Get))
            {
                var content = result.Content.ReadAsStringAsync();
                content.Wait();
                firebaseResponse.JSONContent = content.Result;
            }
            return firebaseResponse;
        }

        public static Task<HttpResponseMessage> RequestHelper(HttpMethod method, Uri uri, string json = null)
        {
            var client = new HttpClient();
            var msg = new HttpRequestMessage(method, uri);
            msg.Headers.Add("user-agent", Constants.getUserAgent());
            if (json != null)
            {
                msg.Content = new StringContent(
                    json,
                    UnicodeEncoding.UTF8,
                    "application/json");
            }

            return client.SendAsync(msg);
        }
        public FirebaseRequest(HttpMethod method, string uri, string jsonString = null)
        {
            Method = method;
            JSON = jsonString;
            Uri = uri;
        }
    }
}
