﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Models
{
    public class Book
    {
        public string id { get; set; }

        public string selfLink { get; set; }

        public string title { get; set; }

        public string subtitle { get; set; }

        public int pageCount { get; set; }

        public string thumbnail { get; set; }
    }
}
