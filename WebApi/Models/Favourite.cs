﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Models
{
    public class Favourite
    {
        [JsonProperty]
        public string favouriteGuid { get; set; }

        public string favouriteId { get; set; }

        [JsonProperty]
        public string bookDescription { get; set; }

        [JsonProperty]
        public string bookPhotoUrl { get; set; }

        [JsonProperty]
        public string userId { get; set; }

        [JsonProperty]
        public string bookId { get; set; }

        [JsonProperty]
        public int pageCount { get; set; }

        [JsonProperty]
        public string bookUrl { get; set; }

        [JsonProperty]
        public string bookName { get; set; }
    }
}
