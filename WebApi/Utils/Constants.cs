﻿namespace WebApi.Utils
{
    public static class Constants
    {
        private const string SearchBookUrl = "https://www.googleapis.com/books/v1/volumes?q=";

        private const string FirebaseDatabaseUrl = "https://lyncastest-1581614385286.firebaseio.com/favourites/";

        private const string USER_AGENT = "firebase-net/1.0";

        public static string getSearchBookUrl()
        {
            return SearchBookUrl;
        }

        public static string getFirebaseDatabaseUrl() {
            return FirebaseDatabaseUrl;
        }

        public static string getUserAgent()
        {
            return USER_AGENT;
        }
    }
}
