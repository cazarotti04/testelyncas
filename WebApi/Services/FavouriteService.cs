﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WebApi.Firebase;
using WebApi.Models;
using WebApi.Utils;

namespace WebApi.Services
{
    public class FavouriteService
    {
        #region Singleton
        private static FavouriteService instance;

        private FavouriteService() { }

        public static FavouriteService Instance
        {
            get
            {
                if (instance == null)
                    lock (typeof(FavouriteService))
                        if (instance == null) instance = new FavouriteService();

                return instance;
            }
        }
        #endregion

        public FirebaseResponse NewFavourite(Favourite newFavourite)
        {
            FirebaseResponse response = new FirebaseResponse();
            try
            {
                if (String.IsNullOrEmpty(newFavourite.favouriteGuid))
                {
                    string guid = Guid.NewGuid().ToString();
                    newFavourite.favouriteGuid = guid;
                }

                string valid = "";

                valid = PostValidate(newFavourite);

                if (!String.IsNullOrEmpty(valid))
                {
                    response.ErrorMessage = valid;
                    response.Success = false;
                    return response;
                }

                string json = JsonConvert.SerializeObject(newFavourite);

                response = new FirebaseRequest(HttpMethod.Post, Constants.getFirebaseDatabaseUrl() + ".json", json).Execute();

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public FirebaseResponse Favourites(string userId)
        {
            if (String.IsNullOrEmpty(userId))
                return new FirebaseResponse(false, new String("O Id do usuário não foi informado"));

            try
            {
                FirebaseResponse response = new FirebaseRequest(HttpMethod.Get, Constants.getFirebaseDatabaseUrl() + ".json").Execute();

                response.JSONContent = response.JSONContent.Replace("\n", "");
                char[] delimiters = { '{', '}' };
                var splits = response.JSONContent.Split(delimiters);
                List<Favourite> lista = new List<Favourite>();
                for (int aux = 2; aux < splits.Length; aux++)
                {
                    if (!String.IsNullOrWhiteSpace(splits[aux]))
                    {
                        if (splits[aux][0] != ',')
                        {
                            var _object = JsonConvert.DeserializeObject<Favourite>(new String("{" +splits[aux] + "}"));
                            if(_object.userId == userId)
                            {
                                string teste = splits[aux - 1].Split('"')[1];
                                _object.favouriteId = teste;
                                lista.Add(_object);
                            }
                                
                        }
                    }
                }

                response.objRetorno = lista;

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public FirebaseResponse DeleteFavourite(string FavouriteGuid, string userId)
        {
            bool trigger = false;
            FirebaseResponse response = new FirebaseResponse();
            try
            {
                if (String.IsNullOrEmpty(FavouriteGuid))
                {
                    response.ErrorMessage = "O guid do Favourite model não foi informado";
                    response.Success = false;
                    return response;
                }
                if (String.IsNullOrEmpty(userId))
                {
                    response.ErrorMessage = "O Id do usuário não foi informado";
                    response.Success = false;
                    return response;
                }

                response = Favourites(userId);

                string favouriteId = "";

                foreach(var item in response.objRetorno as List<Favourite>)
                {
                    if(item.favouriteGuid == FavouriteGuid)
                    {
                        favouriteId = item.favouriteId;
                        trigger = true;
                        break;
                    }
                }

                //Evitar que se não achar um Id apague o banco inteiro
                if(trigger)
                    response = new FirebaseRequest(HttpMethod.Delete, Constants.getFirebaseDatabaseUrl() + favouriteId + ".json").Execute();

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string PostValidate(Favourite newFavourite)
        {
            if (String.IsNullOrEmpty(newFavourite.bookId))
                return new String("O livro deve ter um Id");
            if (String.IsNullOrEmpty(newFavourite.bookName))
                return new String("O livro deve ter um nome");
            if (String.IsNullOrEmpty(newFavourite.bookPhotoUrl))
                return new String("O livro deve ter uma url da foto");
            if (String.IsNullOrEmpty(newFavourite.bookUrl))
                return new String("O livro deve ter uma url");
            if (String.IsNullOrEmpty(newFavourite.favouriteGuid))
                return new String("O objeto deve ter um Guid");
            if (String.IsNullOrEmpty(newFavourite.userId))
                return new String("O userId deve ser informado");

            return "";
        }

    }
}
