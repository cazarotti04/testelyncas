﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Models;
using System.Net.Http;
using WebApi.Utils;
using WebApi.Firebase;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace WebApi.Services
{
    public class SearchService
    {
        #region Singleton
        private static SearchService instance;

        private SearchService() { }

        public static SearchService Instance
        {
            get
            {
                if (instance == null)
                    lock (typeof(SearchService))
                        if (instance == null) instance = new SearchService();

                return instance;
            }
        }
        #endregion

        public FirebaseResponse SearchVolumes(string APIKEY, string bookName, string authorName = "")
        {
            string BookSearchUrl = Constants.getSearchBookUrl();
            string fullURL;
            FirebaseResponse response = new FirebaseResponse();
            try
            {
                if (String.IsNullOrEmpty(APIKEY))
                {
                    response.ErrorMessage = "APIKEY não foi informada";
                    response.Success = false;
                    return response;
                }
                if (String.IsNullOrEmpty(bookName))
                {
                    response.ErrorMessage = "O nome do livro não foi informado";
                    response.Success = false;
                    return response;
                }

                fullURL = BookSearchUrl + bookName;
                if (!String.IsNullOrEmpty(authorName))
                {
                    fullURL += "+inauthor:" + authorName;
                }
                fullURL += "&key=" + APIKEY;

                response = new FirebaseRequest(HttpMethod.Get, fullURL + "&max-results=40").Execute();
                JToken token = JObject.Parse(response.JSONContent);

                var items = token.SelectToken("items");
                var lista = new List<Book>();

                foreach(var item in items)
                {
                    try
                    {
                        var x = JsonConvert.DeserializeObject<Volume>(item.ToString());
                        var book = new Book()
                        {
                            id = x.id,
                            pageCount = x.volumeInfo.pageCount,
                            selfLink = x.volumeInfo.previewLink,
                            subtitle = x.volumeInfo.description,
                            thumbnail = x.volumeInfo.imageLinks.thumbnail,
                            title = x.volumeInfo.title
                        };
                        lista.Add(book);
                    }
                    catch
                    {
                        continue;
                    }
                    
                }
                response.objRetorno = lista;
                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}