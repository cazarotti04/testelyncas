﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApi.Firebase;
using WebApi.Models;
using WebApi.Services;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SearchController : Controller
    {
        [HttpGet]
        public ActionResult SearchByName(string auth, string bookName, string authorName="")
        {
            FirebaseResponse retorno = new FirebaseResponse();
            try
            {
                retorno = SearchService.Instance.SearchVolumes(auth, bookName, authorName);

                return Json(retorno);
            }
            catch (Exception ex)
            {
                retorno.ErrorMessage = ex.Message;
                retorno.Success = false;
                return Json(retorno);
            }
        }
    }
}