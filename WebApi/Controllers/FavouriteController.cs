﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApi.Firebase;
using WebApi.Models;
using WebApi.Services;

namespace WebApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class FavouriteController : Controller
    {
        [HttpPost]
        public ActionResult NewFavourite(Favourite newFavourite)
        {
            FirebaseResponse retorno = new FirebaseResponse();
            try
            {
                retorno = FavouriteService.Instance.NewFavourite(newFavourite);

                return Json(retorno);
            }
            catch (Exception ex)
            {
                retorno.ErrorMessage = ex.Message;
                retorno.Success = false;
                return Json(retorno);
            }
        }

        [HttpGet]
        public ActionResult Favourites(string userId)
        {
            FirebaseResponse retorno = new FirebaseResponse();
            try
            {
                retorno = FavouriteService.Instance.Favourites(userId);

                return Json(retorno);
            }
            catch (Exception ex)
            {
                retorno.ErrorMessage = ex.Message;
                retorno.Success = false;
                return Json(retorno);
            }
        }

        [HttpDelete]
        public ActionResult DeleteFavourite(string favouriteGuid, string userId)
        {
            FirebaseResponse retorno = new FirebaseResponse();
            try
            {
                retorno = FavouriteService.Instance.DeleteFavourite(favouriteGuid, userId);

                return Json(retorno);
            }
            catch (Exception ex)
            {
                retorno.ErrorMessage = ex.Message;
                retorno.Success = false;
                return Json(retorno);
            }
        }

    }
}
