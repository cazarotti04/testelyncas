import { Component, OnInit } from '@angular/core';
import { User } from 'src/assets/models/user.model';
import { AuthServiceC } from '../services/auth.service';
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private auth: AuthServiceC, private router: Router) { }

  bookName:string = "";

  UserData: User = JSON.parse(localStorage.getItem("UserData"));

  ngOnInit() {
    console.log(this.UserData)
  }

  GoogleLogout(){
    this.auth.signOut();
  }

  search(value){
    this.bookName = value.target.elements[0].value;
    this.router.navigateByUrl('search/' + this.bookName);
  }

}
