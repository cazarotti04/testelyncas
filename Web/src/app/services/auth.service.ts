import { Injectable } from '@angular/core';
import { AppRoutingModule } from '../app-routing/app-routing.module';

import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreDocument} from '@angular/fire/firestore';
import { auth } from 'firebase/app'

import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { User } from '../../assets/models/user.model'
import { Router } from '@angular/router';
// import { AuthService, SocialUser, GoogleLoginProvider } from 'angularx-social-login'

@Injectable({
  providedIn: 'root'
})
export class AuthServiceC {

  user$: Observable<User>;  

  constructor(private router: Router, private afAuth: AngularFireAuth, private afs: AngularFirestore) { 
    this.user$ = this.afAuth.authState.pipe(
      switchMap(user => {
        if(user){
          return this.afs.doc<User>(`users/${user.uid}`).valueChanges();
        }
        else{
          return of(null); 
        }
      })
    );
  }

  async googleLogin(){
    const provider = new auth.GoogleAuthProvider();
    const credential = await this.afAuth.auth.signInWithPopup(provider);
    return this.updateUserData(credential.user);
  }

  async signOut() {
    await this.afAuth.auth.signOut();
    return this.router.navigate(['/']);
  }

  private updateUserData({uid, email, displayName, photoURL} : User){

    const userRef: AngularFirestoreDocument<User> = this.afs.doc(`users/${uid}`);

    const data ={
      uid: uid,
      email: email,
      displayName: displayName,
      photoURL: photoURL
    };

    localStorage.setItem("UserData", JSON.stringify(data));

    return userRef.set(data, { merge : true});
  }


}
