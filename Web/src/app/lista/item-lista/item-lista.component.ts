import { Component, OnInit, Input } from '@angular/core';
import { Favourite } from "../../../assets/models/favourite.model";

@Component({
  selector: 'app-item-lista',
  templateUrl: './item-lista.component.html',
  styleUrls: ['./item-lista.component.css']
})
export class ItemListaComponent implements OnInit {

  @Input() volume:Favourite;

  constructor() { }

  ngOnInit() {
  }

}
