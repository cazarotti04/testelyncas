import { Component, OnInit } from '@angular/core';
import { Http, Response } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { Favourite } from "../../assets/models/favourite.model";
import Swal from 'sweetalert2';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.css']
})
export class ListaComponent implements OnInit {

  constructor(private http: HttpClient) { }

  retorno:string;
  lista:Favourite[] = [];
  data:any;

  columns = ["bookDescription", "bookId", "bookPhotoUrl", "bookUrl", "favouriteGuid", "pageCount", "userId"];

  ngOnInit() {
    this.searchFavourites();
  }

  searchFavourites(){
    this.http.get("http://localhost:5000/favourite").subscribe( (data:any) => {
      this.retorno = data['jsonContent'];

      let teste = JSON.parse(this.retorno);

      var result = Object.keys(teste).map(function(key) {
        return teste[key];
      });
      this.lista = result;
    });
  }

  deleteFavourite(favouriteGuid:string, userId:string){
    this.http.delete("http://localhost:5000/favourite?favouriteGuid=" + favouriteGuid + "&userId=" + userId).subscribe( (data:any) => {
      console.log(data);
      if(data.success){
        Swal.fire("Ok!", "O livro foi removido dos seus favoritos", "success");
      }
      if(!data.success){
        Swal.fire("Ops", "Houve algo de errado ao remover este livro dos seus favoritos", "warning");
      }
      this.searchFavourites();
    });
  }

  openModal(volume){
    Swal.fire({
      title: 'Remover ' + volume.bookName + ' da sua lista de favoritos?',
      text: "Depois você pode adiciona-lo de novo à sua lista pesquisando pelo livro!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Remover',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) {
        this.deleteFavourite(volume.favouriteGuid, volume.userId);
      }
    })
  }

}
