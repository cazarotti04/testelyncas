import { Component } from '@angular/core';
import { AuthServiceC } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(public auth: AuthServiceC){}
  title = 'Web';
}
