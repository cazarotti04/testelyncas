import { Component, OnInit } from '@angular/core';
import { AuthServiceC } from '../../services/auth.service'

@Component({
  selector: 'app-login-card',
  templateUrl: './login-card.component.html',
  styleUrls: ['./login-card.component.css']
})
export class LoginCardComponent implements OnInit {

  constructor(private auth: AuthServiceC) { }

  ngOnInit() {
  }

  googleLogin(){
    this.auth.googleLogin();
  }

}
