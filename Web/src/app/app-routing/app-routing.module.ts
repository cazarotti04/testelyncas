import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from '../login/login.component'
import { DashboardComponent } from '../dashboard/dashboard.component';
import { SearchComponent } from "../search/search.component";
import { Page404Component } from "../page404/page404.component";

    const routes: Routes = [
        {
            path: 'dashboard',
            component: DashboardComponent,
        },
        { 
            path: '',
            redirectTo: 'dashboard',
            pathMatch: 'full'
        },
        {
            path: 'search/:bookName',
            component: SearchComponent,
        },
        {
            path: 'login',
            component: LoginComponent,
        },
        { 
            path: '*',
            redirectTo: 'Page404Component',
            pathMatch: 'full'
        },
    ];

    @NgModule({
        imports: [
            RouterModule.forRoot(routes)
        ],
        exports: [
            RouterModule
        ],
        declarations: []
    })
    export class AppRoutingModule { }