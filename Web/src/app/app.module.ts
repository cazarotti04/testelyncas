import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';

import { AppRoutingModule } from './app-routing/app-routing.module';
import { LoginCardComponent } from './login/login-card/login-card.component';
import { AngularFireAuthModule } from '@angular/fire/auth'
import {  AngularFireDatabaseModule } from '@angular/fire/database'
import { AngularFireModule } from '@angular/fire'
import { AngularFirestore } from '@angular/fire/firestore';
import { HeaderComponent } from './header/header.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { ListaComponent } from './lista/lista.component';
import { ItemListaComponent } from './lista/item-lista/item-lista.component';
import { HttpModule } from "@angular/http";
import { HttpClientModule } from "@angular/common/http";
import { SearchComponent } from './search/search.component';
import { Page404Component } from './page404/page404.component';


var firebaseConfig = {
  apiKey: "AIzaSyCqSxp1L1NJivvpE_SMvjkGnlRwngLHMDc",
  authDomain: "lyncastest-1581614385286.firebaseapp.com",
  databaseURL: "https://lyncastest-1581614385286.firebaseio.com",
  projectId: "lyncastest-1581614385286",
  storageBucket: "lyncastest-1581614385286.appspot.com",
  messagingSenderId: "70189963559",
  appId: "1:70189963559:web:5780428168f31c984db102"
};

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LoginCardComponent,
    HeaderComponent,
    DashboardComponent,
    ListaComponent,
    ItemListaComponent,
    SearchComponent,
    Page404Component,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFontAwesomeModule,
    HttpModule,
    HttpClientModule
  ],
  providers: [
    AngularFirestore
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
