import { Component, OnInit, Input } from '@angular/core';
import { Book } from 'src/assets/models/book.model';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.css']
})
export class ListaComponent implements OnInit {

  @Input() volume:Book;
  
  constructor() { }

  ngOnInit() {
    console.log(this.volume)
  }

}
