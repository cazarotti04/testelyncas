import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { User } from 'src/assets/models/user.model';

import "../../assets/consts";
import { getSecret } from '../../assets/consts';
import { ListaComponent } from "../search/lista/lista.component";
import { Book } from 'src/assets/models/book.model';
import { Favourite } from 'src/assets/models/favourite.model';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  sub: any;
  bookName:any;
  retorno:any;
  lista:Array<Book>;

  UserData: User = JSON.parse(localStorage.getItem("UserData"));
  constructor(private route: ActivatedRoute, private http: HttpClient) { }

  ngOnInit() {
    this.bookName = this.route.snapshot.paramMap.get('bookName');
    this.searchBookByName();
  }

  searchBookByName(){
    this.http.get("http://localhost:5000/search?auth=" + getSecret() + "&bookName=" + this.bookName).subscribe( (data:any) => {
      this.retorno = data.objRetorno;
      this.lista = this.retorno;
      this.lista.forEach(element => {
        if(element.title.length > 40)
          element.title = element.title.substring(0,40) + "..."
        if(element.subtitle)
          element.subtitle = element.subtitle.substring(0,80) + "...";
      });
    });
  }

  novoFavorito(volume:Book){
    let favourite:Favourite = {
      bookDescription : volume.subtitle,
      bookId: volume.id,
      bookName: volume.title,
      bookPhotoUrl: volume.thumbnail,
      bookUrl: volume.selfLink,
      favouriteGuid: "",
      pageCount: volume.pageCount,
      userId: this.UserData.uid
    }

    this.http.post("http://localhost:5000/favourite", favourite).subscribe( (data:any) => {
      if(data.success){
        Swal.fire("Ok!", "O livro foi adicionado aos seus favoritos", "success");
      }
      else{
        Swal.fire("Ops!", "Houve um erro ao adicionar o livro aos seus favoritos :B", "error");
      }
    });
  }

}
