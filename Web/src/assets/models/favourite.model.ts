export interface Favourite {
    bookDescription: string;
    bookId: string;
    bookPhotoUrl: string;
    bookUrl: string;
    favouriteGuid: string;
    pageCount: Number;
    userId: string;
    bookName: string;
}