export interface Book {
    id: string;
    selfLink: string;
    title: string;
    subtitle: string;
    pageCount: Number;
    thumbnail: string;
}